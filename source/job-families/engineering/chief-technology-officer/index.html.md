---
layout: job_family_page
title: "Chief Technology Officer"
---

## Responsibilities

* Call attention to problems with our features, data-model, work-flow, test suit, installation process, and upgrade process.
* The CTO has a veto on all features,
the [CEO will also be involved in many decisions](http://www.bhorowitz.com/why_founders_fail_the_product_ceo_paradox).
* Ensure [software quality](https://en.wikipedia.org/wiki/Software_quality).
* Start new projects that might materially affect the scope and future of the company.
* Serve as a fresh set of eyes on new functionality, and help to make it usable and known.
* For an explanation how this is different from the VP role please see [CTO vs VP Engineering: What’s the Difference?](https://www.ivyexec.com/executive-insights/2015/cto-versus-vp-engineering-whats-the-difference/)
* Must successfully complete a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

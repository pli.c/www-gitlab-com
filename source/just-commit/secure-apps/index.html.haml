---
layout: just-commit
title: Just Commit - Secure Apps
description: Discover how to automate security testing, and ensure every bit of code is scanned before it leaves the developer’s hands.
suppress_header: true
extra_css:
  - just-commit.css
extra_js:
  - just-commit.js
---

.just-commit-hero
  .hero-content#just-commit-hero
    .commit-grid-cover
    .just-commit-label
      = partial "includes/just-commit/just-commit-logo.svg"
    %h1 To delivering secure apps.
    %p Stay out of headlines. Discover how to automate security testing, and ensure every bit of code is scanned before it leaves the developer’s hands.
    %a.btn.cta-btn.just-commit-button.margin-top20{ href: "/resources/whitepaper-seismic-shift-application-security/" }
      Learn how

.toc-links
  %a{ href: "#integrated-security" } Integrated security
  %a{ href: "#life-stories" } Real life stories
  %a{ href: "#continuous-security" } Continuous security testing with GitLab
  %a{ href: "#solutions" } A single solution
  %a{ href: "#app-security" } Dive deeper

.just-commit-content-container
  .content#integrated-security
    .content-column-container
      .content-column
        %p.title-label INTEGRATED SECURITY
        %h2 Lean in to DevSecOps. Commit to better business velocity.
        %p When security testing is separate from the development process, it slows down the velocity and blocks application delivery. By the time the resource-constrained security team to run the scans, developers have already moved into another project and have to spend significant time understanding the context of the vulnerability. As a result, business improvements take a back seat.
        %p Balancing business velocity with security is possible. When application security is a natural byproduct of the development workflow, every piece of code is tested upon commit, developers can remediate in real-time, security teams focus on the high priority vulnerabilities, and third party code is identifiable.
        -# Put slider here
        %p Suddenly, speed and security aren’t at odds with each other. You’re able to increase velocity, reduce cycle time, minimize risk, and deliver greater business agility without needing to make harmful tradeoffs.
      .content-column
        .download-cta
          .z-index1
            .cta-icon
              = partial "includes/just-commit/cta-icon-secure.svg"
            %h3.margin-top0 A Seismic Shift in Application Security
            %p Download the whitepaper now to find out how to integrate and automate security in the DevOps lifecycle.
            %a.cta-btn.btn.just-commit-button{ href: "/resources/whitepaper-seismic-shift-application-security/" }
              Download the whitepaper
          .cta-bg
            = partial "includes/just-commit/just-commit-cta-bg.svg"

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  .content
    .stats-container
      .stat
        %h3.margin-top0 Just commit to integrated security testing.
        %p With application security embedded into the developers workflow, security bugs are found earlier, clearing noise for the security analysts and, ultimately, increasing time to market.
        %p Vulnerabilities are often discovered in production, caught just before go-live, causing project delays. But, with application security baked into the CI/CD process, every merge request is scanned for vulnerabilities in the code and its dependencies. Developers are able take action without waiting for security and learn from common mistakes, allowing security teams to focus on anomalies and exceptions. As a result, less time is spent on remediating security flaws and security is no longer a blocker to application delivery.
      .stat
        %h3.margin-top0 Just commit to reducing technical debt.
        %p The use of third-party code is exploding: The percentage of open source code within a codebase has grown 21% in just one year*. As more third-party code is introduced, usage increases, and risk compounds. Not catching a dependency vulnerability in real-time can cause major setbacks later.
        %p However, when built into the CI/CD pipeline, a dependency scanner can analyze external dependencies (e.g. Ruby gem libraries) for known vulnerabilities on each code commit. Imagine that as a developer is coding, they are getting notifications, in real-time, when a dependency is vulnerable, and a link to patch it immediately.
    .stats-container#big-stats
      .stat.stat-big
        .stat-bar
        %h1 96%
        %p of applications contain open source components*
      .stat.stat-big
        .stat-bar
        %h1 57%
        %p of a codebase is open source code, on average.*
      .stat.stat-big
        .stat-bar
        %h1 78%
        %p of codebases examined contained at least one vulnerability*
    %p.text-center.font-small.margin-top30 *Source: Black Duck’s 2018 Open Source Security and Risk Analysis report.
    .stats-container
      .stat
        %h3.margin-top0 Just commit container security.
        %p Containers are a great solution for making applications transportable and reducing infrastructure configuration overhead or vendor lock-in. However, they also introduce an entirely new surface that is vulnerable to attacks. Automotive company, Tesla, discovered this the hard way last year when a Kubernetes container that had been left without password protection was discovered and attacked by a hacker.
        %p Thankfully, container scanning can be integrated directly into the development workflow so developers can be alerted of container vulnerabilities before code is deployed to production.

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  .content#life-stories
    %p.title-label REAL LIFE STORIES
    .content-column-container.margin-top40
      %a.content-column.tile-resource{ href: "/2019/01/16/wag-labs-blog-post/" }
        .tile-background-container{ style: "background-image: url('/images/blogimages/dog-walking.jpg');" }
        .tile-info
          %h3.tile-title How Wag! cut their release process from 40 minutes to just 6
          %p.tile-link
            Read more
            %i.fas.fa-arrow-right
      %a.content-column.tile-resource{ href: "/customers/equinix/" }
        .tile-background-container{ style: "background-image: url('/images/blogimages/equinix-case-study-image.png');" }
        .tile-info
          %h3.tile-title Equinix increases the agility of their DevOps teams with self-serviceability and automation
          %p.tile-link
            Read more
            %i.fas.fa-arrow-right

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  #moving-lines
    .moving-lines-container
      = partial "includes/just-commit/line-animation-big.svg"
    .moving-lines-content
      .content-tile
        %p.text-center Suddenly, speed and security aren’t at odds with each other. You’re able to increase velocity, reduce cycle time, minimize risk, and deliver greater business agility without needing to make harmful tradeoffs.

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  .content#continuous-security
    %p.title-label SECURE & DEFEND WITH GITLAB
    %h2 Get integrated security out-of-the-box with a single application for the entire DevOps lifecycle.
    %p GitLab’s continuous security testing capabilities support decision makers. Instead of security being a blocker, application security testing is integrated into the continuous integration and delivery process, so every piece of code is automatically tested upon commit, without incremental cost.
    .content-column-container.margin-top50
      .content-column
        .column-accent
          = partial "includes/just-commit/accent-box1.svg"
        %h3.margin-top10 Commit to greater efficiency
        %p Balancing business velocity with security is possible. When application security is a natural byproduct of the development workflow, every piece of code is tested upon commit, developers can remediate in real-time, security teams focus on the high priority vulnerabilities, and third party code is identifiable.
      .content-column
        .column-accent
          = partial "includes/just-commit/accent-box2.svg"
        %h3.margin-top10 Commit to simplicity
        %p GitLab’s single application embeds security testing into developers’ natural workflow with understandable results reported to the developer, including remediation advice and line-of-code detail, enabling them to remove the vulnerability before their code is merged with other code.
      .content-column
        .column-accent
          = partial "includes/just-commit/accent-box3.svg"
        %h3.margin-top10 Commit to compliance
        %p Help your teams achieve and demonstrate compliance with controls built-in to to the workflow. Instead of treating compliance like a cumbersome afterthought, rest easy knowing that auditing, logging, traceability, and reporting are built-in.

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  .content#solutions
    %p.title-label A SINGLE SOLUTION
    .content-column-container
      .content-column-content
        %h3 Security workflow for developers
        %p Developers have a front-row view of security with features baked into the merge request pipeline. GitLab’s integrated static application security testing (SAST) scans to spot potential vulnerabilities before deployment; dynamic application security testing (DAST) runs live attacks against the automatically provided review app. All vulnerabilities are shown in-line with the affected code for easy remediation.
        %a.btn.cta-btn.just-commit-button{ herf: "/solutions/dev-sec-ops/" }
          Learn more
      .content-column-image-container
        .content-column-image
          = partial "includes/just-commit/images/security-scanning.svg"
    .content-column-container
      .content-column-image-container
        .content-column-image
          = partial "includes/just-commit/images/security-visibility.svg"
      .content-column-content
        %h3 Visibility into third party code and containers
        %p Dependency scanning and container scanning is built-in to GitLab’s CI/CD pipelines. Automatically analyze external dependencies for known vulnerabilities, check application environments, and check license compliance with every commit—without any additional set up.
        %a.btn.cta-btn.just-commit-button{ herf: "/solutions/dev-sec-ops/" }
          Learn more
    .content-column-container
      .content-column-content
        %h3 High-level view of overall risk from applications
        %p GitLab’s Security Dashboard gives security professionals a single view of the status of each branch by aggregating vulnerabilities found in each merge request. This view gives security teams a high-level view so they can easily take action on the highest-priority issues.
        %a.btn.cta-btn.just-commit-button{ herf: "/2018/09/14/inside-gitlab-security-dashboards/" }
          Learn more
      .content-column-image-container
        .content-column-image
          = partial "includes/just-commit/images/security-risk.svg"

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  .content
    = partial "includes/just-commit/cta/secure-apps"

  .content-divider
    = partial "includes/just-commit/dividers/secure-divider.svg"

  .content.dive-deeper-container#dive-deeper
    %p.title-label DIVE DEEPER
    .dive-deeper-links
      %a.stat{ href: "/resources/whitepaper-seismic-shift-application-security/" }
        %p A seismic shift in application security
      %a.stat{ href: "/solutions/dev-sec-ops/" }
        %p What is continuous security testing?
      %a.stat{ href: "/solutions/compliance/" }
        %p Building compliant applications
      %a.stat{ href: "/solutions/pci-compliance/" }
        %p PCI compliance with GitLab
      %a.stat{ href: "/solutions/financial-services-regulatory-compliance/" }
        %p Financial services regulatory compliance with GitLab
      %a.stat{ href: "/solutions/hipaa-compliance/" }
        %p HIPAA compliance with GitLab

  %script{src: "/javascripts/libs/tweenmax.min.js", type: "text/javascript"}

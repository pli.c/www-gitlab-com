---
layout: markdown_page
title: "Evangelist Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

At GitLab our mission is to change all creative work from read-only to read-write so that **everyone can contribute**. In order to fulfill this mission, we need to create both the tools and platform to enable this change and a community of contributors who share our mission. We are just getting started in building the GitLab community and we encourage everyone to contribute to its growth. 

There are many ways to participate in the GitLab community today: [contributing to an open source project](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/), [contributing to our documentation](https://docs.gitlab.com/ee/development/documentation/), [hosting your open source project on GitLab](https://about.gitlab.com/solutions/open-source/), or teaching your colleagues and collaborators about the value of [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/). 

We are building an evangelist program to support people who share our mission and want to give tech talks, run local meetups, or create videos or blogs. We will be announcing more in Q1. For now, please email `evangelists@gitlab.com` if you have feedback on our vision, ideas for how we can build our community, or suggestions for a name for our evangelist program. 

## Supporting community events

We'd love to support you if you are organizing an event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.). Depending on the number and type of attendees at an event, it may be owned by [Corporate Marketing](https://about.gitlab.com/handbook/marketing/#new-ideas-for-marketing-campaigns-or-events), [Field Marketing](https://about.gitlab.com/handbook/marketing/marketing-sales-development/field-marketing/#evaluating-potential-field-initiatives), or Community Relations. Our events decision tree can help you find the right team to handle an event request. 

Events Decision Tree:
![event decision tree](/images/handbook/marketing/event-decision-tree.png)

To submit a community event for support or sponsorship:

1. Review our events decision tree to ensure you are directing your event inquiry to the appropriate team.
1. Send an email to `evangelists@gitlab.com` with the event details including event name, contact name, contact email, event URL, number of attendees, and other relevant information. A ticket will be created on the [Evangelist Program issue tracker](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues) which you can use to track progress on the event. 
1. GitLab XDRs: for *contact requests* received in Salesforce via the [Sales webform](https://about.gitlab.com/sales/) requesting event sponsorship, please change ownership to `GitLab Evangelist` in SFDC & be sure to "check the box" to send a notification.
1. GitLab's Evangelist Program Manager will review the request and follow up with the event contact.

## Organizing meetups

- We love and support meetups. If you participate in local tech groups and are interested in having a GitLab speaker or GitLab as a sponsor, please submit an [event issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=events). Please note, providing sufficient lead time (at least a month) allows us to better support your event so feel free to reach out as soon as possible. 
- If you are interested in creating your own GitLab meetup or if you already have an existing meetup on meetup.com that you would like to link to GitLab's meetup.com Pro account, please email `evangelists@gitlab.com`.  You can find the list of GitLab meetups on the [meetup.com page](https://www.meetup.com/pro/gitlab). 
- When you are getting started, we recommend scheduling at least 2 meetups. Publish your first meeting with a date and topic, and then a second meeting with a date and topic. The second meeting can have a flexible topic based on how the first meeting goes. The point of setting two meet-ups is to help build momentum in the new group.  
- Ideally, the first couple of meetups will include GitLab employees. Once the group has hosted a couple of successful events, it becomes easier for the group to sustain itself. It is much harder to start new meetups versus maintaining existing ones. So we make an effort to support and keep existing events going.
- Reach out to other like-topic meetups and invite them to your meetup.
- Once you have scheduled your meetup, add the event to our [events page](/events/) so the whole world knows! Check out the [How to add an event to the events page](/handbook/marketing/corporate-marketing/#how-to-add-events-to-the-aboutgitlabcomevents-page) section if you need help on how to add an event.
- If you purchase any food & beverage for the meetup event, we can help reimburse the expense.  A general guideline is $US 5/person for a maximum of $US 500 per each meetup. You will be asked to provide receipts, attendees list/photo, etc.  If you have questions or need help with food & beverage reimbursements, please email `evangelists@gitlab.com`.

## Speakers for community events

We'd love to support you if you are organizing an event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.).

You can get in touch with speakers from the GitLab team and the wider community to participate and do a talk at your event. We maintain a list of active speakers on our [Find a GitLab speaker](https://about.gitlab.com/events/find-a-speaker/) page. Once you find a speaker in your region, contact them directly. For GitLabbers, you can also check the #cfp channel on Slack where many of our active tech speakers will see your speaker request. Most speakers will also be able to do talks remotely if the event is virtual or if travel is a challenge.

If you have questions, you can always reach us by sendind an e-mail to `evangelists@gitlab.com`.

## Becoming a tech speaker

If you are aware of people from the GitLab community who are interested in giving a tech talk relating to GitLab, please direct them to our [Become a Speaker](https://about.gitlab.com/community/evangelists/become-a-speaker/) page for more information on the type of support we provide. 

For GitLabbers who want to become a tech speaker, contact `evangelists@gitlab.com` and check out the #cfp channel on Slack to discover upcoming opportunities. Additional detail on the logistics of giving a talk once your proposal has been accepted can be found on the [Corporate Marketing](https://about.gitlab.com/handbook/marketing/corporate-marketing/#speakers) page.

## Contributing content

GitLab actively supports content contributors. Our community team tracks GitLab content and our evangelist program manager and editorial team regularly reviews the content.  If you would like to submit your content for review, please create an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=community-content) on our evangelist program project.

We make an effort to amplify and support content contributions that generate value for our community. Criteria we consider include: how well a post addresses an issue in the Community Writers issue tracker, how well a post aligns with our strategy and values, and how well a post is written. 

As we identify posts that meet our criteria, we decide how we want to support the posts. We may also identify creators who we want to partner with on content. Writers who are looking for inspiration may want to visit our [Community Writers issue tracker](https://gitlab.com/gitlab-com/community-writers/issues) which tracks blog post ideas submitted by our community.

You can track the status of community content submissions on the Community Content board. Submissions go through the following steps: 

1. Review: review submissions for accuracy.
1. Say thanks: contact the creators to say thank you, share a swag code, and highlight other ways they can contribute. $25/creator is the standard amount. 
1. Share: Post on social (with a credit to the author) or retweet.  
1. Syndicate: for posts that we love or that answer a common or important question, we may ask the author if we can add the post to our Medium publication or the GitLab blog.
1. Curate: creators who have shown a depth of experience around topics important to GitLab and the wider GitLab community may be asked to submit talks or posts about said topics. 

### Blog

Coming soon. Contact `evangelists@gitlab.com` if you have any questions. 
 
### Videos

Coming soon. Contact `evangelists@gitlab.com` if you have any questions. 

## Helpful Resources

- [Merchandise](/handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html)
- [Find a speaker](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker.html)

## Operations

- [Merchandise](/handbook/marketing/community-relations/evangelist-program/ops/merchandise.html)
---
layout: markdown_page
title: Importing Projects for Customers
category: GitLab.com
---

### On this page
{:.no_toc}

- TOC
{:toc}

---

## Overview

This workflow is meant to provide guidance on when GitLab Team members might offer to import projects on behalf of customers or prospects as a courtesy, and the process for doing the imports.

Due to the shifting nature of what issues might be relevant, the specifics of this workflow may frequently change.

## Criteria

The Support Team can offer to import a few projects as a best effort courtesy, but anything complex with a hundreds of users or projects is out of scope. These users should be referred to [Professional Services](https://about.gitlab.com/services/migration/).

Additionally:

1. The requestor should be an existing customer or a prospect.
1. The import was attempted and failed one or more times.
1. The import method is a GitLab project export file.

## General Process

Before offering to do the import, verify the following:

1. User is a group owner.
1. User has rights to create new projects in the space.

If not verified, let the user know we might be able to offer an import, but a group owner needs to contact us or they need to adjust their settings (depending on which of the above is not verified).

Once verified:

1. Offer for the GitLab team to do an import on their behalf as a courtesy.
1. Ask the user to confirm:
    1. the group where the project should be imported,
    1. that _all_ users have accounts on GitLab.com with matching email address or username,
    1. provide a "default" user for items that cannot be mapped to existing users,
    1. no project currently exists in the namespace with the project's name, and
    1. to provide a copy of their project's export.
1. When receiving the project export: download, and ensure it unpacks. If not, ask the user for another copy.
1. Assuming success, respond to the user that we have the export and will send an update when the import is complete.
1. Upload the file and pass on a temporary secure link in a confidential issue (see below for details). Recommended sites:
    - send.firefox.com (up to 1GB)
    - wetransfer.com (up to 2 GB)
1. Delete any local copies of the export once it's passed on.

### Correctly Mapped Users

Currently, an admin is required to currently map users in GitLab as [per our documentation](https://docs.gitlab.com/ee/user/project/settings/import_export.html). There is a [feature proposal to not require an admin user](https://gitlab.com/gitlab-org/gitlab-ce/issues/36338) being discussed.

The [process is still being discussed](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1406), so what is below is currently a draft workflow.

Follow the general process, then:

1. File an [issue in dotcom-escalations](https://gitlab.com/gitlab-com/support/dotcom/dotcom-escalations/issues) with all relevant information using the `SE Escalation` template.
1. SE will do the import and re-map any relevant items to the "default" user provided.
1. SE will close the issue when complete.
1. Let the user know the import is done and they should double check its completeness.

### Large Projects

Due to [CE issue #52956](https://gitlab.com/gitlab-org/gitlab-ce/issues/52956), users with large projects (~1GB+) will often see partial imports.

Assuming the user meets the criteria, follow the general process then:

1. Fill in the `import` template on the [infra issue tracker](https://gitlab.com/gitlab-com/gl-infra/infrastructure) with all necessary information including the link to the export and the ZD link for reference.
1. A message will be sent to #announcements channel on Slack when done.
1. Let the user know the import is done and they should double check its completeness.

### Other Cases

When in doubt, file an issue in [dotcom-escalations](https://gitlab.com/gitlab-com/support/dotcom/dotcom-escalations/issues) and ask for a manager's input.

If a manager approves, proceed with the import or if it requires console access, mark as `SE Escalation`.
